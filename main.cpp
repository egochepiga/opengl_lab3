#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include "bmpLoader.h"

GLfloat xRotated, yRotated, zRotated;
GLdouble base = 1;
GLdouble height = 1.5;
GLint slices = 50;
GLint stacks = 50;

#define PI 3.1415926535898
#define Cos(th) cos(PI/180*(th))
#define Sin(th) sin(PI/180*(th))
#define DEF_D 5

/*  Globals */
double dim = 9.0;
const char *windowName = "Lab 2";
int windowWidth = 600;
int windowHeight = 550;
int toggleAxes = 0;
int toggleValues = 1;
int toggleMode = 0;   /* projection mode */
int th = 20;
int ph = 20;   /* elevation of view angle */
int fov = 55; /* field of view for perspective */
int asp = 1;  /* aspect ratio */
double LookAtX = 0.0;
double LookAtY = 0.0;
double LookAtZ = 0.0;
float sphereZ = 1.0; // Morphing
int objId = 0;
double light1PositionY = 0.0;
double light0PositionX = 0.0;

GLuint LoadTexture(const char * filename);
void drawAxes();
void drawShape();
void drawCylinder();
void ligh();
void display();
void project();
void setEye(double x, double y, double z);
void reshape(int width, int height);
void windowKey(unsigned char key, int x, int y);
void windowSpecial(int key, int x, int y);

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(windowWidth, windowHeight);
    glutCreateWindow(windowName);
    LoadTexture("rb.bmp");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(windowKey);
    glutSpecialFunc(windowSpecial);
    glutMainLoop();
    return 0;
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glLoadIdentity();
    setEye(LookAtX, LookAtY, LookAtZ);

    drawAxes();
    drawCylinder();
    drawShape();
    ligh();

    glFlush();
    glutSwapBuffers();
}

void ligh(){
    float diffuseWhite[4] = { 1, 1, 1, 1 };
    float specularWhite[4] = { 1, 1, 1, 1 };
    GLfloat light0_position[] = { light0PositionX, 0.0, 2.0, 0.0 };

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularWhite);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseWhite);
    glLightfv(GL_LIGHT0, GL_POSITION, light0_position);

    float diffuseColor[4] = { 0.8, 0.2, 0.5, 1 };
    float specularColor[4] = { 1, 1, 1, 1 };
    GLfloat light1_position[] = { 0.0, light1PositionY, 2.0, 0.0 };

    glEnable(GL_LIGHT1);
    glLightfv(GL_LIGHT1, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuseColor);
    glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
    glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.0);
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.2);
    glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.4);

    /*
    glEnable(GL_LIGHTING);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuseColor);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 100.0);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specularColor);
*/
     }

void drawAxes() {
    if (toggleAxes) {
        double len = 2.0;
        glColor3f(1.0, 1.0, 1.0);
        glBegin(GL_LINES);
        glVertex3d(0, 0, 0);
        glVertex3d(len, 0, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, len, 0);
        glVertex3d(0, 0, 0);
        glVertex3d(0, 0, len);
        glEnd();
        glRasterPos3d(len, 0, 0);
        glRasterPos3d(0, len, 0);
        glRasterPos3d(0, 0, len);
    }
}

void drawShape() {

    glTranslatef(4, 0, 0);
    glutSolidCube(3);
    glTranslatef(4, 0, 0);
    glColor4f(0, 1, 0, 1);
    glutSolidSphere(2, 16, 16);

}

void drawCylinder() {
    glColor4f(1.0,1.0,1.0,0.5);
    glScalef(-1.0f, 1.0f, 1.0f);
    /* top triangle */
    double i, resolution  = 0.01;
    double height = 6;
    double radius = 2;

    glPushMatrix();
    glTranslatef(0, -0.5, 0);

    glBegin(GL_TRIANGLE_FAN);
    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(0, height, 0);  /* center */
    for (i = 2 * PI; i >= 0; i -= resolution)

    {
        glTexCoord2f( 0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f );
        glVertex3f(radius * cos(i), height, radius * sin(i));
    }
    /* close the loop back to 0 degrees */
    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(radius, height, 0);
    glEnd();

    /* bottom triangle: note: for is in reverse order */
    glBegin(GL_TRIANGLE_FAN);
    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(0, 0, 0);  /* center */
    for (i = 0; i <= 2 * PI; i += resolution)
    {
        glTexCoord2f( 0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f );
        glVertex3f(radius * cos(i), 0, radius * sin(i));
    }
    glEnd();

    /* middle tube */
    glBegin(GL_QUAD_STRIP);
    for (i = 0; i <= 2 * PI; i += resolution)
    {
        const float tc = ( i / (float)( 2 * PI ) );
        glTexCoord2f( tc, 0.0 );
        glVertex3f(radius * cos(i), 0, radius * sin(i));
        glTexCoord2f( tc, 1.0 );
        glVertex3f(radius * cos(i), height, radius * sin(i));
    }
    /* close the loop back to zero degrees */
    glTexCoord2f( 0.0, 0.0 );
    glVertex3f(radius, 0, 0);
    glTexCoord2f( 0.0, 1.0 );
    glVertex3f(radius, height, 0);
    glEnd();

    glPopMatrix();
}

void project() {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fov, asp, dim / 4, 4 * dim);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void setEye(double x, double y, double z) {

    double Ex = -2 * dim*Sin(th)*Cos(ph);
    double Ey = +2 * dim        *Sin(ph);
    double Ez = +2 * dim*Cos(th)*Cos(ph);
    gluLookAt(Ex + x, Ey + y, Ez + z, 0, 0, 0, 0, Cos(ph), 0);

}

void reshape(int width, int height) {
    asp = (height>0) ? (double)width / height : 1;
    glViewport(0, 0, width, height);
    project();
}

void windowKey(unsigned char key, int x, int y) {

    if (key == 32) {
        if (objId == 4) objId = 0;
        else objId++;
    }

    else if (key == '-' && key>1) fov--;
    else if (key == '+' && key<179) fov++;
    else if (key == 'a') dim += 0.1;
    else if (key == 'd' && dim>1) dim -= 0.1;
    else if (key == 'x') sphereZ += 0.1;
    else if (key == 'z') sphereZ -= 0.1;
    else if (key == 'o') light1PositionY += 0.1;
    else if (key == 'p') light1PositionY -= 0.1;
    else if (key == 'l') light0PositionX += 0.1;
    else if (key == ';') light0PositionX -= 0.1;

    project();
    glutPostRedisplay();
}

void windowSpecial(int key, int x, int y) {
    if (key == GLUT_KEY_RIGHT) th += 5;
    else if (key == GLUT_KEY_LEFT) th -= 5;
    else if (key == GLUT_KEY_UP) ph += 5;
    else if (key == GLUT_KEY_DOWN) ph -= 5;


    th %= 360;
    ph %= 360;

    project();
    glutPostRedisplay();
}

GLuint LoadTexture(const char * filename){
    unsigned int ID;
    bmpLoader bl(filename);
    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &ID);
    glBindTexture(GL_TEXTURE_2D, ID);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, bl.iWIdth, bl.iHeight, GL_RGB, GL_UNSIGNED_BYTE, bl.textureData);

}