//
// Created by egor on 17.04.18.
//

#include "bmpLoader.h"
#include <stdio.h>
#include <iostream>

bmpLoader::bmpLoader(const char* filename) {
    FILE *file = fopen(filename,"rb");
    if (!file)
        std::cout << "OMG" << std::endl;
    fread(&bfh,sizeof(BITMAPFILEHEADER),1,file);
    if (bfh.bfType != 0x4D42 )
        std::cout << "wrong format" << std::endl;
    fread(&bih, sizeof(BITMAPINFOHEADER),1,file);
    if (bih.biSizeImage == 0)
        bih.biSizeImage = bih.biHeight * bih.biWidth * 3;
    textureData = new unsigned char[bih.biSizeImage];
    fseek(file, bfh.bfOffBits, SEEK_SET);
    fread(textureData, 1, bih.biSizeImage, file);
    unsigned char tmp;
    for (int i = 0; i < bih.biSizeImage; i+=3) { //3 bytes at 1 time BGR to RGB
        tmp = textureData[i];
        textureData[i] = textureData[i+2];
        textureData[i+2] = tmp;
    }
    iWIdth = bih.biWidth;
    iHeight = bih.biHeight;
    fclose(file);
}

bmpLoader::~bmpLoader() {
    delete [] textureData;
}